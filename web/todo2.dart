import 'dart:html';

import 'package:todo2/todo_model.dart';
import 'dart:convert' show JSON;

App app;

void main() {
  var container = querySelector("#todo_container");
  app = new App(container, window.localStorage);
  
  InputElement inputField = querySelector('#inputTodo');
  inputField.onKeyUp.listen(addTodo);
  
}

void addTodo(Event e) {
  if((e as KeyboardEvent).keyCode == KeyCode.ENTER)
  {
    var target = (e.target as InputElement);
    app.addTodo(target.value);
    target.value = "";
  }
}
