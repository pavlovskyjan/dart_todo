library todo_model;

import 'dart:html';
import 'dart:convert' show JSON;

final String STORAGE_KEY = 'todoList';

class App {
  List<Todo> _todos;

  Element _element;
  Storage storage;

  App(this._element, Storage this.storage) {
    _todos = new List();
    initData();
  }

  List<Todo> get todos => _todos;


  Todo addTodo(String text) {
    var todo = new Todo.withDom(_element, text);
    _todos.add(todo);
    todo.app = this;
    save();
    return todo;
  }
  
  initData(){
    if(window.localStorage[STORAGE_KEY] != null){
      var decoded = JSON.decode(window.localStorage[STORAGE_KEY]);
          decoded.forEach((e) => addTodoJson(e)); 
    } 
  }
 
  addTodoJson(Map json){
    print(json["t"]);
    var todo = addTodo(json["t"]);
    todo.done = json["d"];
  }
  
  toJson(){
    return JSON.encode(_todos);
  }
  save(){
    window.localStorage[STORAGE_KEY] = toJson(); 
  }
  
  deleteTodo(Todo todo){
    _todos.remove(todo);
    save();
  }
  
}

class Todo{
  String _text;
  final HtmlElement _element;
  bool _done = false;
  App app;
  

  Todo(HtmlElement this._element, String this._text);

  factory Todo.withDom(Element container, String text) {
    var deleteElement = new ButtonElement()
        ..className = "close"
        ..text = "×";

    var todoElement = new DivElement()..className = "todo alert alert-danger";
    var element = new DivElement()
        ..className = "todo-item"
        ..text = text;
    todoElement.children.addAll([deleteElement, element]);

    container.children.insert(0, todoElement);
    var todo = new Todo(todoElement, text);
    todoElement.onDoubleClick.listen((e) => todo.toggleDone());
    deleteElement.onClick.listen((e) => todo.delete());
    return todo;
  }
  
  void set done(bool done) {
    _done = done;
    if (done) {
      _element.classes
          ..remove("alert-danger")
          ..add("alert-success");
    } else {
      _element.classes
          ..remove("alert-success")
          ..add("alert-danger");
    }
    app.save();
  }

  void delete() {
    _element.remove();
    app.deleteTodo(this);
  }

  void toggleDone() {
    this.done = !_done;
  }

  void set text(String s) {
    _text = s;
  }

  Map toJson() => {"t": _text, "d": _done};
  
  String get text => _text;

  String toString() => _text;
}
